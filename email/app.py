from operator import imod
import ssl, pika, smtplib
from pymongo import MongoClient
from time import sleep

sleep(10)

port = 587
smtp_server = "smtp.gmail.com"
sender_email = "wartolds@gmail.com"
password = 'dljmmeovyzxnxjyf'
message = """\
Subject: New point

A new public point was added to the map:"""

context = ssl.create_default_context()

def get_emails():
    CONNECTION_STRING = "database:27017"
    client = MongoClient(CONNECTION_STRING)
    db = client['refugeedb']
    users = db['Users']
    return [ user['email'] for user in users.find()]

def pdf_process_function(msg):
    s_msg = msg.decode("utf-8")
    
    mails = get_emails()

    for receiver_email in mails:
        with smtplib.SMTP(smtp_server, port) as server:
            server.starttls(context=context)
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message + s_msg )
            server.quit()

def callback(ch, method, properties, body):
    pdf_process_function(body)

url = 'amqp://rabbit'

params = pika.URLParameters(url)
connection = pika.BlockingConnection(params)
channel = connection.channel()
channel.queue_declare(queue='points')

channel.basic_consume('points', callback, auto_ack=True)

channel.start_consuming()
connection.close()